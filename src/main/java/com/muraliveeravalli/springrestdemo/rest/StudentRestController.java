package com.muraliveeravalli.springrestdemo.rest;

import com.muraliveeravalli.springrestdemo.entity.Student;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class StudentRestController {

    private List<Student> students;

    @PostConstruct
    public void loadDate() {
        students = new ArrayList<>();
        students.add(new Student("Murali", "Manohar"));
        students.add(new Student("Amrutha", "Veeravalli"));
        students.add(new Student("Aramudhan", "Veeravalli"));
    }


    @GetMapping("/students")
    public List<Student> getStudents() {
        return students;
    }

    @GetMapping("/students/{studentId}")
    public Student getStudent(@PathVariable int studentId) {
        try {
            return students.get(studentId);
        } catch (IndexOutOfBoundsException e) {
            throw new StudentNotFoundException("Student with id " + studentId + ", not found", e);
        }
    }

}
